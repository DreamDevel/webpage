<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/header.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/footer.php");

	headerbar("Dream Development - eRadio","eradio");
?>

<div id="eradio-page">
	<div class="wrapper1">
		<div class="container">

			<img src=<?= base_url("/media/images/eRadioSSb2-e.png") ?>>

			<div class="content">

				<h1>eRadio</h1>
				<div class="subtitle">A minimalist and powerfull radio player for elementary OS</div>

				<div class="buttons-box">
					<div class="button-container">
						<div class="button-darkgrey">Download for free</div>
						<div class="button-subtitle">And donate later?</div>
					</div>

					<div class="button-container">
						<div class="button-blue">Download for $0.99</div>
						<div class="button-subtitle">And support us!</div>
					</div>
				</div>

				<!-- Code Box -->
				<div class="footer">
					<div>Current eRadio Version: 2.0</div>
					<div>Curious about next version ? <a href=<?= base_url("/apps/eradio/roadmap.php") ?>> Check the roadmap</a></div>
				</div>

			</div>
		</div>
	</div>

	<div class="info-header">Features</div>

	<div class="info-box-row"><!--

		--><div class="info-box">
			<div class="title">Radio From Your Desktop</div>
			<i class="fa fa-desktop"></i>
			<div class="description">Keep in one place all your favorite stations and play them via a native elementary OS application</div>
		</div><!--

		--><div class="info-box">
			<div class="title">Beutiful Design</div>
			<i class="fa fa-heart"></i>
			<div class="description">eRadio is designed espeasially for elementary OS with minimalist in mind</div>
		</div><!--
		--><div class="info-box">
			<div class="title">Stream Everything</div>
			<i class="fa fa-play-circle"></i>
			<div class="description">By using gstreamer we support several stream types. eRadio also supports pls, asx and m3u</div>
		</div><!--
		--><div class="info-box" style="margin:0;">
			<div class="title">Buckup & Share</div>
			<i class="fa fa-file"></i>
			<div class="description">Export your radio stations easily to eradio packages to buckup your library or share it</div>
		</div><!--

	--></div>

	<div class="info-box-row" style="padding-bottom:50px;"><!--

		--><div class="info-box">
			<div class="title">Categorized Stations</div>
			<i class="fa fa-list"></i>
			<div class="description">Categorize your stations via genre to easily find them via eRadio's left side bar</div>
		</div><!--

		--><div class="info-box">
			<div class="title">Media Keys Integration</div>
			<i class="fa fa-keyboard-o"></i>
			<div class="description">Switch, play and pause stations without interupting your workflow</div>
		</div><!--
		--><div class="info-box">
			<div class="title">Notifications Support</div>
			<i class="fa fa-bell"></i>
			<div class="description">eRadio uses elementary OS native notifications to inform you what station you are listening to</div>
		</div><!--
		--><div class="info-box" style="margin:0;">
			<div class="title">Free And Open Source</div>
			<i class="fa fa-github"></i>
			<div class="description">Try eRadio and decide if it is worth paying for it. Read and hack the eradio's source code</div>
		</div><!--

	--></div>

	<div class="wrapper-involved">
		<hr>
		<div class="info-header" style="margin-bottom:40px;">Get Invovled</div>

		<div class="box">
			<div class="info-header-small">Translate</div>
			<i class="fa fa-globe"></i>
			<div class="details">Translate eRadio to your native language to let more people be able to use it </div>
			<a href="https://translations.launchpad.net/eradio">Go to Launchpad</a>
		</div><!--
	 --><div class="box">
			<div class="info-header-small">Bug Report</div>
			<i class="fa fa-bug"></i>
			<div class="details">Did you found a bug ? Report it to help us improve your eRadio experience</div>
			<a href="https://github.com/DreamDevel/eRadio/issues">Go to Github</a>
		</div><!--
	 --><div class="box" style="margin:0;">
			<div class="info-header-small">Feedback</div>
			<i class="fa fa-users"></i>
			<div class="details">Tell us what you think! Send us what you like what you don't and ideas about the future of eRadio</div>
			<a href=<?= base_url("/contact.php") ?>>Contact us</a>
		</div>
	</div>
</div>

<?php footer(); ?>