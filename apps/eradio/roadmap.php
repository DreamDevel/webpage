<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/header.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/footer.php");

	headerbar("Dream Development - eRadio","eradio");
?>

<?php function print_task($task,$status) {

    $status_color = "default";
    if ($status == "Done")
        $status_color = "green";
    else if ($status == "In Progress")
        $status_color = "orange";
    else if ($status == "Not Started")
        $status_color = "red";

    echo "<tr>";
    echo "<td>" . $task . "</td>";
    echo "<td style='text-align:center;" . ($status_color=="default" ? "" : "color:$status_color;") . "'>" . $status . "</td>";
    echo "</tr>";
}

?>

<div id="eradio-roadmap-page">
	<div class="wrapper1">
        <div style="font-size:28px;text-align:center;"><a href=<?= base_url("/apps/eradio") ?>>eRadio's</a> roadmap</div>
        <div style="font-size:18px;text-align:center;">Where we stand and where we go</div>

        <div style="height:50px;"></div>
        <h4 class="version-header">Version 2.0</h4>
        <table>
            <thead>
                <tr>
                    <th class="task">Task</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    print_task("Redesign interface (Main window & dialogs)", "In Progress");
                    print_task("Code Refactoring. Modular & Clean Code","In Progress");
                    print_task("New database schema with genres","Done");
                    print_task("Replace sqlite by sqlheavy","Done");
                    print_task("Add Station filtering by genre in sidebar","Done");
                    print_task("Create Sidebar & add current genres","Done");
                    print_task("Create Progress dialog for import/export stations", "In Progress");
                    print_task("Create Donate Dialog", "Not Started");
                    print_task("Create new icon", "Not Started");
                ?>
            </tbody>
        </table>


        <div style="height:50px;"></div>
        <h4 class="version-header">Version 2.1</h4>
        <table>
            <thead>
                <tr>
                    <th class="task">Task</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    print_task("Implement station providers", "Not Started");
                ?>
            </tbody>
        </table>


        <div style="height:50px;"></div>
        <h4 class="version-header">Version 2.2</h4>
        <table>
            <thead>
                <tr>
                    <th class="task">Task</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    print_task("Create socket-based interface for mobile app communication", "Not Started");
                    print_task("Create 'eRadio remote' android app", "Not Started");
                ?>
            </tbody>
        </table>
	</div>
</div>

<?php footer(); ?>

