<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/header.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/footer.php");

	headerbar("Dream Development - Blog","blog");

	/* Temporary Posts */

	$posts = [];
	$titles = ["Why we don't support other distros",
			   "eRadio version 1.3 released",
			   "eRadio version 1.3 Beta released. Ready for translations",
			   "Our teams workspace",
			   "Website Up and Running"];

	for ($i =0 ; $i < 5; $i++) {

		$obj = new stdClass ();
		$obj->title = $titles[$i];
		$obj->content = "Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man power to support other distros...";
		array_push($posts,$obj);
	}


	?>
<div id="blog-page" style="background-color:#136CB0;min-height:100%;color:white;">

	<div class="wrapper1" style="width:1024px;margin:0 auto;padding-top:70px;">
		<div class="title" style="height:28px;font-size:24px;text-shadow: 1px 1px 3px rgba(60, 60, 90, 1);text-align:center;">
		<div style="display:inline-block;height:1px;background-color:white;width:350px;margin-right:40px;vertical-align:middle;"></div>News, Articles & Logs<div style="margin-left:40px;vertical-align:middle;display:inline-block;height:1px;background-color:white;width:350px;"></div>
		</div>

		<div class="posts" style="padding-bottom:70px;padding-top:48px;">

			<div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "Why we don't support other destros" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Fri 25 Mar 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div><!--

			--><div style="width:84px;height:0px;display:inline-block;"></div><!--

			--><div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "eRadio version 1.3 released" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Tue 21 Mar 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div>

			<div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "eRadio v1.3 beta released, ready for translations" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Sut 17 Mar 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div><!--

			--><div style="width:84px;height:0px;display:inline-block;"></div><!--

			--><div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "Do it like Dream Developers" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Mon 11 Mar 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div>

			<div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "Help us Help you" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Wed 5 Mar 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div><!--

			--><div style="width:84px;height:0px;display:inline-block;"></div><!--

			--><div class="post" style="width:470px;display:inline-block;border-bottom:1px solid white;padding-bottom:48px;padding-top:48px;">
				<div class="title" style="font-size:16px;font-weight:bold;"><?= "Website Up And Running!" ?></div>
				<div class="content" style="font-size:14px;margin-top:24px;margin-bottom:24px;">Currently we support only elementary OS. Some people may don't like this, espesially those that using other distros. Dream Development Team is parted currently from 2 people.  There is not enough man  power to support other distros...</div>
				<div class="info">
					<div class="date" style="font-size:12px;display:inline-block;">Mon 22 Feb 2015</div>
					<div class="" style="font-size:12px;display:inline-block;float:right;">Read More</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php

	footer();

?>