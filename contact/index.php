<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/header.php");
	require($_SERVER["DOCUMENT_ROOT"] . "/templates/footer.php");

	headerbar("Dream Development - Contact","contact");
?>

<div id="contact-page">
	<div class="wrapper1">
		<div class="sub-wrapper">
			<div class="header-medium margin-bottom-medium">Contact us</div>
			<div class="contact_description margin-bottom-medium">If you have a question, a suggestion or
				 anything else you would like to tell us do
				 not hesitate to contact us. You can send as
				 email directly or use the contact form.
			</div>
			<div class="margin-bottom-large">
				Email: support@dreamdevel.com
			</div>

			<div class="header margin-bottom-medium">Social Networks</div>
			<div class="social-icons">
				<i class="fa fa-google-plus-square" onclick="window.location='https://twitter.com/DreamDevel'"></i>
				<i class="fa fa-twitter-square" onclick="window.location='https://twitter.com/DreamDevel'"></i>
			</div>
		</div><!--
	 --><div class="sub-wrapper" style="width:400px;padding-left:112">
	 		<div class="header-medium margin-bottom-medium">Contact Form</div>
	 		<form>
	 		<div class="margin-bottom-small">
	 			<div class="input-label">Name:</div>
	 			<input type="text">
	 		</div>

	 		<div class="margin-bottom-small">
	 			<div class="input-label">Email:</div>
	 			<input type="text">
	 		</div>

 				<textarea style="width:400px;height:300px;" placeholder="Enter your message"></textarea>
 				<div class="button-blue margin-top-medium" style="height:18px;padding-top:2px;float:right;">Send</div>
 			</form>
	</div>
</div>

<?php footer(); ?>