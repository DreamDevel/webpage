<?php

function base_path($path) {
	return $_SERVER["DOCUMENT_ROOT"] . $path;
}

function base_url($path,$include_quotes = true) {

	$final_url = "";

	if ($include_quotes)
		$final_url .= '"';

	$final_url .= "http://" . $_SERVER['SERVER_NAME'] . $path;

	if ($include_quotes)
		$final_url .= '"';

	return $final_url;
}

?>