<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");



function headerbar($title = "Dream Development",$menu_selected = "") {  ?>
	<html>
		<head>
		 	<title><?= $title ?></title>
		 	<link rel="stylesheet" href=<?= base_url("/css/style.css") ?>>
		 	<link rel="stylesheet" href=<?= base_url("/css/font-awesome.min.css") ?>>

		 	<link rel="icon"
		 	      type="image/png"
		 	      href=<?= base_url ("/media/images/favicon.png") ?> >
		</head>
		<body>

		<nav class="headerbar">
			<div class="content">

				<img class="logo" src=<?= base_url("/media/images/logo.png");?>>
				<div class="title" onclick=<?= "window.location=" . base_url("/") ?>>Dream Development</div>

				<ul>
					<li <?= $menu_selected == "welcome" ? "class='selected'" : ""; ?> onclick=<?= "window.location=" . base_url("/") ?>>Welcome</li>
					<li <?= $menu_selected == "eradio" ? "class='selected'" : ""; ?> onclick=<?= "window.location=" . base_url("/apps/eradio") ?>>eRadio</li>
					<li <?= $menu_selected == "blog" ? "class='selected'" : ""; ?> onclick=<?= "window.location=" . base_url("/blog") ?>>Blog</li>
					<li <?= $menu_selected == "contact" ? "class='selected'" : ""; ?> onclick=<?= "window.location=" . base_url("/contact") ?>>Contact</li>
					<li <?= $menu_selected == "about" ? "class='selected'" : ""; ?> onclick=<?= "window.location=" . base_url("/about") ?>>About</li>
				</ul>

			</div>
		</nav>

<?php	} ?>