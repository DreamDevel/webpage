<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/templates/global.php");
	require(base_path("/templates/header.php"));
	require(base_path("/templates/footer.php"));


	headerbar("Dream Development - Welcome","welcome");
?>

<div id="welcome-page">

	<div class="wrapper1">
		<div class="eradio-advert">
			<div class="title">eRadio</div>
			<div class="subtitle">A minimalist and powerfull radio player for elementary OS</div>
			<div class="link">
				<a href=<?= base_url ("/apps/eradio") ?> >Go to eRadio page to learn more</a>
			</div>
			<img src=<?= base_url("/media/images/welcome-eradio.png") ?>>
		</div>
	</div>

	<div class="wrapper2">
		<div class="block">

			<div class="header">What's New</div>

			<div class="post">
				<div class="title">Why we don't support other distros</div>
				<div class="details">
					<div class="date">Sun 15 Feb 2015</div>
					<div class="link"><a href="">Go to blog post</a></div>
				</div>
			</div>

			<div class="post">
				<div class="title">eRadio version 1.3 released</div>
				<div class="details">
					<div class="date">Tue 28 Jan 2015</div>
					<div class="link"><a href="">Go to blog post</a></div>
				</div>
			</div>

			<div class="post">
				<div class="title">eRadio version 1.3 ready for translations</div>
				<div class="details">
					<div class="date">Mon 15 Dec 2014</div>
					<div class="link"><a href="">Go to blog post</a></div>
				</div>
			</div>

			<div class="post">
				<div class="title">How To : Grab radio url from webpage</div>
				<div class="details">
					<div class="date">Wed 22 Sep 2014</div>
					<div class="link"><a href="">Go to blog post</a></div>
				</div>
			</div>

			<div class="post">
				<div class="title">Website Up And Running!</div>
				<div class="details">
					<div class="date">Tue 10 Sep 2014</div>
					<div class="link"><a href="">Go to blog post</a></div>
				</div>
			</div>

		</div><!--
	 --><div class="space"></div><!--
	 --><div class="block-large">
	 		<div class="header">Support us</div>
	 		<div class="content">
	 			<p>Many applications created for linux are low quality. One of the reasons is that many developers have a primary job and they don't have
	 			much time to spend on their applications.</p><p>Dream Development team decided to work full time on linux applications and make a living with
	 			donations. Consider donating to keep us going.</p>
	 			<div class="button-blue button-margin">Make A Donation</div>
	 		</div>
	    </div><!--
	 --><div class="space"></div><!--
	 --><div class="block">
	 		<div class="header">Twitter</div>
	 		<a class="twitter-timeline" href="https://twitter.com/DreamDevel" data-widget-id="516931525967753216" data-chrome="noheader nofooter" width="260" height:"260" >Tweets by @DreamDevel</a>
	 	</div>
	</div>
</div>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<?php footer(); ?>